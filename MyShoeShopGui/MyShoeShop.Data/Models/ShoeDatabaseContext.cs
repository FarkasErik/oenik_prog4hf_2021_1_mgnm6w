﻿// <copyright file="ShoeDatabaseContext.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

#nullable disable

namespace MyShoeShop.Data.Models
{
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// ShoeDatabaseContext data type.
    /// </summary>
    public partial class ShoeDatabaseContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShoeDatabaseContext"/> class.
        /// </summary>
        public ShoeDatabaseContext()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShoeDatabaseContext"/> class.
        /// </summary>
        /// <param name="options">options for the context.</param>
        public ShoeDatabaseContext(DbContextOptions<ShoeDatabaseContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Gets or sets the suppliers in the db.
        /// </summary>
        public virtual DbSet<Beszallito> Beszallitos { get; set; }

        /// <summary>
        /// Gets or sets the shoes in the db.
        /// </summary>
        public virtual DbSet<Cipo> Cipos { get; set; }

        /// <summary>
        /// Gets or sets the custumers of the db.
        /// </summary>
        public virtual DbSet<Vasarlo> Vasarlos { get; set; }

        /// <inheritdoc/>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=D:\\University\\Prog4HF\\MyShoeShopGui\\MyShoeShop.Data\\ShoeDatabase.mdf;Integrated Security=True");
            }
        }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Beszallito>(entity =>
            {
                entity.ToTable("Beszallito");

                entity.Property(e => e.BeszallitoId)
                    .HasColumnType("numeric(20, 0)")
                    .HasColumnName("Beszallito_ID");

                entity.Property(e => e.AlapitasDatuma)
                    .HasColumnType("datetime")
                    .HasColumnName("Alapitas_Datuma");

                entity.Property(e => e.EvesBevetel)
                    .HasColumnType("numeric(20, 0)")
                    .HasColumnName("Eves_Bevetel");

                entity.Property(e => e.Nepszeruseg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nev)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Szekhely)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Cipo>(entity =>
            {
                entity.ToTable("Cipo");

                entity.Property(e => e.CipoId)
                    .HasColumnType("numeric(20, 0)")
                    .HasColumnName("Cipo_ID");

                entity.Property(e => e.Ar).HasColumnType("numeric(20, 0)");

                entity.Property(e => e.BeszallitoId)
                    .HasColumnType("numeric(20, 0)")
                    .HasColumnName("Beszallito_ID");

                entity.Property(e => e.Fajta)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Meret).HasColumnType("numeric(20, 0)");

                entity.Property(e => e.Nev)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Szin)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Beszallito)
                    .WithMany(p => p.Cipos)
                    .HasForeignKey(d => d.BeszallitoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("beszallito_fk");
            });

            modelBuilder.Entity<Vasarlo>(entity =>
            {
                entity.HasKey(e => e.Email)
                    .HasName("vasarlo_pk");

                entity.ToTable("Vasarlo");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CipoId)
                    .HasColumnType("numeric(20, 0)")
                    .HasColumnName("Cipo_ID");

                entity.Property(e => e.Lakcim)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Magassag).HasColumnType("numeric(20, 0)");

                entity.Property(e => e.Nem)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nev)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SzuletesiDatum)
                    .HasColumnType("date")
                    .HasColumnName("Szuletesi_datum");

                entity.HasOne(d => d.Cipo)
                    .WithMany(p => p.Vasarlos)
                    .HasForeignKey(d => d.CipoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("cipo_fk");
            });

            this.OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
