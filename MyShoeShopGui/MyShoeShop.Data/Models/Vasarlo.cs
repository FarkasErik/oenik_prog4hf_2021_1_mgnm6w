﻿// <copyright file="Vasarlo.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

#nullable disable

namespace MyShoeShop.Data.Models
{
    using System;

    /// <summary>
    /// Customer data type.
    /// </summary>
    public partial class Vasarlo
    {
        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        public string Nev { get; set; }

        /// <summary>
        /// Gets or sets the date of birth of the customer.
        /// </summary>
        public DateTime SzuletesiDatum { get; set; }

        /// <summary>
        /// Gets or sets the address of the customer.
        /// </summary>
        public string Lakcim { get; set; }

        /// <summary>
        /// Gets or sets the height of the customer.
        /// </summary>
        public decimal Magassag { get; set; }

        /// <summary>
        /// Gets or sets the sex of the customer.
        /// </summary>
        public string Nem { get; set; }

        /// <summary>
        /// Gets or sets the email of the customer.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the id of the shoe which the customer bought.
        /// </summary>
        public decimal CipoId { get; set; }

        /// <summary>
        /// Gets or sets the shoe which the customer bought.
        /// </summary>
        public virtual Cipo Cipo { get; set; }
    }
}
