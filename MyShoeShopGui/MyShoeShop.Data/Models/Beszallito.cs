﻿// <copyright file="Beszallito.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

#nullable disable

namespace MyShoeShop.Data.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Supplier data type.
    /// </summary>
    public partial class Beszallito
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Beszallito"/> class.
        /// </summary>
        public Beszallito()
        {
            this.Cipos = new HashSet<Cipo>();
        }

        /// <summary>
        /// Gets or sets iD of the supplier.
        /// </summary>
        public decimal BeszallitoId { get; set; }

        /// <summary>
        /// Gets or sets the HG of the supplier.
        /// </summary>
        public string Szekhely { get; set; }

        /// <summary>
        /// Gets or sets the date of founding of the supplier.
        /// </summary>
        public DateTime AlapitasDatuma { get; set; }

        /// <summary>
        /// Gets or sets the popularity of the suppplier.
        /// </summary>
        public string Nepszeruseg { get; set; }

        /// <summary>
        /// Gets or sets the yeary income of the supplier.
        /// </summary>
        public decimal EvesBevetel { get; set; }

        /// <summary>
        /// Gets or sets the name of the supplier.
        /// </summary>
        public string Nev { get; set; }

        /// <summary>
        /// Gets or sets the shoes of the supplier.
        /// </summary>
        public virtual ICollection<Cipo> Cipos { get; set; }
    }
}
