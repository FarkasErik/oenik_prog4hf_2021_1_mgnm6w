﻿// <copyright file="Cipo.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

#nullable disable

namespace MyShoeShop.Data.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// Shoe data type.
    /// </summary>
    public partial class Cipo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Cipo"/> class.
        /// </summary>
        public Cipo()
        {
            this.Vasarlos = new HashSet<Vasarlo>();
        }

        /// <summary>
        /// Gets or sets Id of the Shoe.
        /// </summary>
        public decimal CipoId { get; set; }

        /// <summary>
        /// Gets or sets the name of the shoe.
        /// </summary>
        public string Nev { get; set; }

        /// <summary>
        /// Gets or sets the size of the shoe.
        /// </summary>
        public decimal Meret { get; set; }

        /// <summary>
        /// Gets or sets the type of the shoe.
        /// </summary>
        public string Fajta { get; set; }

        /// <summary>
        /// Gets or sets the color of the shoe.
        /// </summary>
        public string Szin { get; set; }

        /// <summary>
        /// Gets or sets the price of the shoe.
        /// </summary>
        public decimal Ar { get; set; }

        /// <summary>
        /// Gets or sets shoes supplierId.
        /// </summary>
        public decimal BeszallitoId { get; set; }

        /// <summary>
        /// Gets or sets the supplier of the shoe.
        /// </summary>
        public virtual Beszallito Beszallito { get; set; }

        /// <summary>
        /// Gets or sets the shoppers of the shoe.
        /// </summary>
        public virtual ICollection<Vasarlo> Vasarlos { get; set; }
    }
}
