﻿CREATE TABLE Beszallito(
	Beszallito_ID NUMERIC(20) not null,
	Szekhely VARCHAR(50), 
	Alapitas_Datuma DateTime, 
	Nepszeruseg varchar(50), 
	Eves_Bevetel NUMERIC(20),
	Nev varchar(50),
	CONSTRAINT bezsallito_pk PRIMARY KEY(Beszallito_ID)
);

Insert Into Beszallito Values (10, 'New York', '17-NOV-1950', '70%', 30000000000, 'Nike');
Insert Into Beszallito Values (20, 'Chicago', '30-JUN-1960', '65%', 40000000000, 'Puma');
Insert Into Beszallito Values (30, 'Boston', '28-FEB-1999', '60%', 20000000000, 'New Balance');
Insert Into Beszallito Values (40, 'Dallas', '5-DEC-1985', '80%', 50000000000, 'Converse');

CREATE TABLE Cipo(
	Cipo_ID NUMERIC(20) not null,
	Nev varchar(50),
	Meret NUMERIC(20) not null,
	Fajta varchar(50) not null, 
	Szin varchar(50) not null, 
	Ar NUMERIC(20),
	Beszallito_ID NUMERIC(20) not null,
	CONSTRAINT cipo_pk PRIMARY KEY(Cipo_ID),
	CONSTRAINT beszallito_fk FOREIGN KEY(Beszallito_ID) REFERENCES Beszallito(Beszallito_ID)
);

Insert Into Cipo Values (123, 'Air Max', 36, 'FutóCipő', 'Kék', 60000, 10);
Insert Into Cipo Values (124, 'Air Max', 38, 'FutóCipő', 'Piros', 60000, 10);
Insert Into Cipo Values (125, 'Air Max', 40, 'FutóCipő', 'Kék', 60000, 10);
Insert Into Cipo Values (126, 'Air Max', 42, 'FutóCipő', 'Piros', 60000, 10);

Insert Into Cipo Values (137, 'Carina', 42, 'Sneaker', 'White', 20000, 20);
Insert Into Cipo Values (138, 'Carina', 40, 'Sneaker', 'Black', 20000, 20);
Insert Into Cipo Values (139, 'Carina', 37, 'Sneaker', 'Blue', 20000, 20);
Insert Into Cipo Values (136, 'Carina', 36, 'Sneaker', 'Gold', 20000, 20);

Insert Into Cipo Values (144, 'All Star', 42, 'High-Top','White', 40000, 40);
Insert Into Cipo Values (145, 'All Star', 40, 'High-Top','Black', 40000, 40);
Insert Into Cipo Values (146, 'All Star', 44, 'High-Top','Black', 40000, 40);
Insert Into Cipo Values (147, 'All Star', 43, 'High-Top','White', 40000, 40);

Insert Into Cipo Values (155, 'GW500PSB', 42,'SportCipő','White', 10000, 30);
Insert Into Cipo Values (156, 'GW500PSB', 41,'SportCipő','Red', 10000, 30);
Insert Into Cipo Values (157, 'GW500PSB', 43,'SportCipő','Blue', 10000, 30);
Insert Into Cipo Values (158, 'GW500PSB', 45,'SportCipő','Silver', 10000, 30);

CREATE TABLE Vasarlo(
	Nev varchar(50),
	Szuletesi_datum Date, 
	Lakcim varchar(50), 
	Magassag NUMERIC(20),
	Nem varchar(50),
	Email varchar(50),
	Cipo_ID NUMERIC(20) not null,
	CONSTRAINT vasarlo_pk PRIMARY KEY(Email),
	CONSTRAINT chk_email CHECK(EMAIL Like '%@%'),
	CONSTRAINT cipo_fk FOREIGN KEY(Cipo_ID) REFERENCES Cipo(Cipo_ID)
);

Insert Into Vasarlo Values ('Gergő', '20-NOV-1999','Senki Földje',180 , 'Férfi' ,'gergő9999@gmail.com',123);
Insert Into Vasarlo Values ('Jancsi', '22-JAN-1998','Itt-Ott',187 , 'Férfi' ,'jancsi123@gmail.com',144);
Insert Into Vasarlo Values ('Gabi', '15-JUN-2002','DeutschLand',166 , 'Nő' ,'gabikaxxx99@gmail.com',158);
Insert Into Vasarlo Values ('Bori', '9-SEP-1995','Salgótarján',150 , 'Nő' ,'boribú@gmail.com',147);

