﻿// <copyright file="GlobalSuppressions.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

using System;
using System.Diagnostics.CodeAnalysis;

[assembly: CLSCompliant(false)]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "IDK what the warning means", Scope = "member", Target = "~P:MyShoeShop.Data.Models.Beszallito.Cipos")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "IDK what the warning means", Scope = "member", Target = "~P:MyShoeShop.Data.Models.Cipo.Vasarlos")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "IDK what the warning means", Scope = "member", Target = "~M:MyShoeShop.Data.Models.ShoeDatabaseContext.OnConfiguring(Microsoft.EntityFrameworkCore.DbContextOptionsBuilder)")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "IDK what the warning means", Scope = "member", Target = "~M:MyShoeShop.Data.Models.ShoeDatabaseContext.OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder)")]
[assembly: SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = "IDK", Scope = "member", Target = "~P:MyShoeShop.Data.Models.Cipo.Cipo_ID")]
