﻿// <copyright file="IShoeLogic.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

namespace MyShoeShop.Logic
{
    using System.Collections.Generic;
    using MyShoeShop.Data.Models;

    /// <summary>
    /// Interface for logic class.
    /// </summary>
    public interface IShoeLogic
    {
        /// <summary>
        /// Calls the create method of the repo.
        /// </summary>
        /// <param name="shoe"> Shoe object which is to be added. </param>
        void CreateCipo(Cipo shoe);

        /// <summary>
        /// Calls the create method of the repo.
        /// </summary>
        /// <param name="beszallito"> Supplier object which is to be added. </param>
        void CreateBeszallito(Beszallito beszallito);

        /// <summary>
        /// Calls the create method of the repo.
        /// </summary>
        /// <param name="vasarlo"> Customer object which is to be added. </param>
        void CreateVasarlo(Vasarlo vasarlo);

        /// <summary>
        /// Calls the read method if the repo.
        /// </summary>
        /// <param name="cipoID"> ID of the desired shoe. </param>
        /// <returns> Returns the desired Shoe object. </returns>
        Cipo ReadCipo(int cipoID);

        /// <summary>
        /// Calls the read method if the repo.
        /// </summary>
        /// <param name="beszallitoID"> ID of the desired supplier. </param>
        /// <returns> Returns the desired supplier. </returns>
        Beszallito ReadBeszallito(int beszallitoID);

        /// <summary>
        /// Calls the read method if the repo.
        /// </summary>
        /// <param name="email"> Email of the desired customer. </param>
        /// <returns> Return the desired customer object. </returns>
        Vasarlo ReadVasarlo(string email);

        /// <summary>
        /// Calls the readall method of the repo.
        /// </summary>
        /// <returns> Return alls the shoes as a list. </returns>
        List<Cipo> ReadAllCipo();

        /// <summary>
        /// Calls the readall method of the repo.
        /// </summary>
        /// <returns> Returns all the suppliers as a list. </returns>
        List<Beszallito> ReadAllBeszallito();

        /// <summary>
        /// Calls the readall method of the repo.
        /// </summary>
        /// <returns> Returns all the customers as a list. </returns>
        List<Vasarlo> ReadAllVasarlo();

        /// <summary>
        /// Calls the update method of the repo.
        /// </summary>
        /// <param name="cipoID"> ID of the to be updated shoe. </param>
        /// <param name="cipo"> New shoe. </param>
        void UpdateCipo(int cipoID, Cipo cipo);

        /// <summary>
        /// Calls the update method of the repo.
        /// </summary>
        /// <param name="cipoID"> ID of the shoe which is to be replaced. </param>
        /// <param name="ujCipoID"> ID of the new shoe. </param>
        void UpdateVasarloCipo(int cipoID, int ujCipoID);

        /// <summary>
        /// Calls the update method of the repo.
        /// </summary>
        /// <param name="beszallitoID"> ID of the to be updated supplier. </param>
        /// <param name="ujNepszeruseg"> New popularity description of the supplier. </param>
        void UpdateBeszallitoNepszeruseg(int beszallitoID, string ujNepszeruseg);

        /// <summary>
        /// Calls the delete method of the repo.
        /// </summary>
        /// <param name="cipoID"> ID if the to be deleted shoe. </param>
        void DeleteCipo(int cipoID);

        /// <summary>
        /// Calls the delete method of the repo.
        /// </summary>
        /// <param name="beszallitoID"> ID of the to be deleted supplier. </param>
        void DeleteBeszallito(int beszallitoID);

        /// <summary>
        /// Calls the delete method of the repo.
        /// </summary>
        /// <param name="email"> Email of the customer. </param>
        void DeleteVasarlo(string email);
    }
}
