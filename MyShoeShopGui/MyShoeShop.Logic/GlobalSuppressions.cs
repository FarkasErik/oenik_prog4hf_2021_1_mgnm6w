﻿// <copyright file="GlobalSuppressions.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

using System;
using System.Diagnostics.CodeAnalysis;

[assembly: CLSCompliant(false)]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "IDK", Scope = "member", Target = "~M:MyShoeShop.Logic.ShoeLogic.#ctor")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "IDK", Scope = "member", Target = "~M:MyShoeShop.Logic.IShoeLogic.ReadAllCipo~System.Collections.Generic.List{MyShoeShop.Data.Models.Cipo}")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "IDK", Scope = "member", Target = "~M:MyShoeShop.Logic.IShoeLogic.ReadAllBeszallito~System.Collections.Generic.List{MyShoeShop.Data.Models.Beszallito}")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "IDK", Scope = "member", Target = "~M:MyShoeShop.Logic.IShoeLogic.ReadAllVasarlo~System.Collections.Generic.List{MyShoeShop.Data.Models.Vasarlo}")]
