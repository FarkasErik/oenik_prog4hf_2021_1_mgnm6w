﻿// <copyright file="ShoeLogic.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

namespace MyShoeShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using MyShoeShop.Data.Models;
    using MyShoeShop.Repository;

    /// <summary>
    /// Buisness logic for the shoe shop db.
    /// </summary>
    public class ShoeLogic : IShoeLogic
    {
        /// <summary>
        /// Shoe shop repo.
        /// </summary>
        private IShoeShopRepository shoeShopRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShoeLogic"/> class.
        /// </summary>
        public ShoeLogic()
        {
            this.shoeShopRepo = new ShouShopRepository(new ShoeDatabaseContext());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShoeLogic"/> class with repo.
        /// </summary>
        /// <param name="repo"> Shoe shop repo. </param>
        public ShoeLogic(IShoeShopRepository repo)
        {
            this.shoeShopRepo = repo;
        }

        /// <inheritdoc/>
        public void CreateCipo(Cipo shoe)
        {
            this.shoeShopRepo.Create(shoe);
        }

        /// <inheritdoc/>
        public void CreateBeszallito(Beszallito beszallito)
        {
            this.shoeShopRepo.Create(beszallito);
        }

        /// <inheritdoc/>
        public void CreateVasarlo(Vasarlo vasarlo)
        {
            this.shoeShopRepo.Create(vasarlo);
        }

        /// <inheritdoc/>
        public Cipo ReadCipo(int cipoID)
        {
            var cipok = this.shoeShopRepo.ReadAllCipo();
            return cipok.Where(x => x.CipoId == cipoID).FirstOrDefault();
        }

        /// <inheritdoc/>
        public Beszallito ReadBeszallito(int beszallitoID)
        {
            var beszallitok = this.shoeShopRepo.ReadAllBeszallito();
            return beszallitok.Where(x => x.BeszallitoId == beszallitoID).FirstOrDefault();
        }

        /// <inheritdoc/>
        public Vasarlo ReadVasarlo(string email)
        {
            var vasarlok = this.shoeShopRepo.ReadAllVasarlo();
            return vasarlok.Where(x => x.Email.Equals(email, StringComparison.Ordinal)).FirstOrDefault();
        }

        /// <inheritdoc/>
        public List<Cipo> ReadAllCipo()
        {
            return this.shoeShopRepo.ReadAllCipo();
        }

        /// <inheritdoc/>
        public List<Beszallito> ReadAllBeszallito()
        {
            return this.shoeShopRepo.ReadAllBeszallito();
        }

        /// <inheritdoc/>
        public List<Vasarlo> ReadAllVasarlo()
        {
            return this.shoeShopRepo.ReadAllVasarlo();
        }

        /// <inheritdoc/>
        public void UpdateCipo(int cipoID, Cipo cipo)
        {
            this.shoeShopRepo.UpdateCipo(cipoID, cipo);
        }

        /// <inheritdoc/>
        public void UpdateVasarloCipo(int cipoID, int ujCipoID)
        {
            this.shoeShopRepo.UpdateVasarloCipo(cipoID, ujCipoID);
        }

        /// <inheritdoc/>
        public void UpdateBeszallitoNepszeruseg(int beszallitoID, string ujNepszeruseg)
        {
            this.shoeShopRepo.UpdateBeszallitoNepszeruseg(beszallitoID, ujNepszeruseg);
        }

        /// <inheritdoc/>
        public void DeleteCipo(int cipoID)
        {
            this.shoeShopRepo.DeleteCipo(cipoID);
        }

        /// <inheritdoc/>
        public void DeleteBeszallito(int beszallitoID)
        {
            this.shoeShopRepo.DeleteBeszallito(beszallitoID);
        }

        /// <inheritdoc/>
        public void DeleteVasarlo(string email)
        {
            this.shoeShopRepo.DeleteVasarlo(email);
        }
    }
}
