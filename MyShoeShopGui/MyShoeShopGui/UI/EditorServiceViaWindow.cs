﻿// <copyright file="EditorServiceViaWindow.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

namespace MyShoeShopGui.UI
{
    using MyShoeShopGui.BL;
    using MyShoeShopGui.DATA;

    /// <summary>
    /// EditorServiceViaWindow.
    /// </summary>
    internal class EditorServiceViaWindow : IEditorService
    {
        /// <inheritdoc/>
        public bool EditShoe(Cipo p)
        {
            EditorWindow win = new EditorWindow(p);
            return win.ShowDialog() ?? false;
        }
    }
}
