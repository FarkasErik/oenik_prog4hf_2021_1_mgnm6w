﻿// <copyright file="EditorViewModel.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

namespace MyShoeShopGui.VM
{
    using GalaSoft.MvvmLight;
    using MyShoeShopGui.DATA;

    /// <summary>
    /// Viem model for the editor window.
    /// </summary>
    internal class EditorViewModel : ViewModelBase
    {
        private Cipo shoe;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// </summary>
        public EditorViewModel()
        {
            this.shoe = new Cipo();
            if (this.IsInDesignMode)
            {
                this.shoe.Nev = "Airmax";
                this.shoe.Ar = 30000;
            }
        }

        /// <summary>
        /// Gets or sets the shoe which is being edited in the editor window.
        /// </summary>
        public Cipo Cipo
        {
            get { return this.shoe; }
            set { this.Set(ref this.shoe, value); }
        }
    }
}
