﻿// <copyright file="MainViewModel.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

namespace MyShoeShopGui.VM
{
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using MyShoeShopGui.BL;
    using MyShoeShopGui.DATA;

    /// <summary>
    /// View model of the main window.
    /// </summary>
    internal class MainViewModel : ViewModelBase
    {
        private IShoeLogic logic;
        private Cipo shoesSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IShoeLogic>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="logic">Bl logic for the viewmodel.</param>
        public MainViewModel(IShoeLogic logic)
        {
            this.logic = logic;

            this.Shoes = new ObservableCollection<Cipo>();

            if (this.IsInDesignMode)
            {
                Cipo p2 = new Cipo() { Nev = "Airmax" };
                Cipo p3 = new Cipo() { Nev = "Converse" };
                this.Shoes.Add(p2);
                this.Shoes.Add(p3);
            }

            this.AddCmd = new RelayCommand(() => this.logic.AddShoe(this.Shoes));
            this.ModCmd = new RelayCommand(() => this.logic.ModShoe(this.ShoesSelected));
            this.DelCmd = new RelayCommand(() => this.logic.DelShoe(this.Shoes, this.ShoesSelected));
            this.GetAllCmd = new RelayCommand(() => this.logic.GetAllShoes(this.Shoes));
        }

        /// <summary>
        /// Gets or sets the selected shoes.
        /// </summary>
        public Cipo ShoesSelected
        {
            get { return this.shoesSelected; }
            set { this.Set(ref this.shoesSelected, value); }
        }

        /// <summary>
        /// Gets the shoes of the main window.
        /// </summary>
        public ObservableCollection<Cipo> Shoes { get; private set; }

        /// <summary>
        /// Gets command for adding a shoe.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets command for moddding a shoe.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets command for deleting a shoe.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets command for getting all the shoes.
        /// </summary>
        public ICommand GetAllCmd { get; private set; }
    }
}
