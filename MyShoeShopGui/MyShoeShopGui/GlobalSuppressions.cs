﻿// <copyright file="GlobalSuppressions.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

using System;
using System.Diagnostics.CodeAnalysis;

[assembly: CLSCompliant(false)]
[assembly: SuppressMessage("CodeQuality", "IDE0052:Remove unread private members", Justification = "IDK", Scope = "member", Target = "~F:MyShoeShopGui.MainWindow.vM")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", Justification = "Late bound")]