﻿// <copyright file="ShoeLogic.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

namespace MyShoeShopGui.BL
{
    using System.Collections.Generic;
    using GalaSoft.MvvmLight.Messaging;
    using MyShoeShopGui.DATA;

    /// <summary>
    /// ShoeLogic for the whp.
    /// </summary>
    internal class ShoeLogic : IShoeLogic
    {
        private readonly MyShoeShop.Logic.ShoeLogic logic = new ();
        private IEditorService editorService;
        private IMessenger messengerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShoeLogic"/> class.
        /// </summary>
        /// <param name="service">IEditorService.</param>
        /// <param name="messenger">IMessenger.</param>
        public ShoeLogic(IEditorService service, IMessenger messenger)
        {
            this.editorService = service;
            this.messengerService = messenger;
        }

        /// <inheritdoc/>
        public void AddShoe(IList<Cipo> list)
        {
            Cipo newShoe = new ();
            if (this.editorService.EditShoe(newShoe) == true)
            {
                list.Add(newShoe);
                this.logic.CreateCipo(new MyShoeShop.Data.Models.Cipo()
                {
                    CipoId = newShoe.CipoId,
                    Nev = newShoe.Nev,
                    Meret = newShoe.Meret,
                    Fajta = newShoe.Fajta,
                    Szin = newShoe.Szin,
                    Ar = newShoe.Ar,
                    BeszallitoId = newShoe.BeszallitoId,
                });
                this.messengerService.Send("ADD OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("ADD CANCEL", "LogicResult");
            }
        }

        /// <inheritdoc/>
        public void ModShoe(Cipo shoeToModify)
        {
            if (shoeToModify == null)
            {
                this.messengerService.Send("EDIT FAILED", "LogicResult");
                return;
            }

            Cipo clone = new Cipo();
            clone.CopyFrom(shoeToModify);
            if (this.editorService.EditShoe(clone) == true)
            {
                shoeToModify.CopyFrom(clone);
                this.logic.UpdateCipo(clone.CipoId, new MyShoeShop.Data.Models.Cipo()
                {
                    CipoId = clone.CipoId,
                    Nev = clone.Nev,
                    Meret = clone.Meret,
                    Fajta = clone.Fajta,
                    Szin = clone.Szin,
                    Ar = clone.Ar,
                    BeszallitoId = clone.BeszallitoId,
                });
                this.messengerService.Send("EDIT OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("EDIT CANCEL", "LogicResult");
            }
        }

        /// <inheritdoc/>
        public void DelShoe(IList<Cipo> list, Cipo shoe)
        {
            if (shoe != null && list.Remove(shoe))
            {
                this.logic.DeleteCipo(shoe.CipoId);
                this.messengerService.Send("DELETE OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("DELETE FAILED", "LogicResult");
            }
        }

        /// <inheritdoc/>
        public IList<Cipo> GetAllShoes(IList<Cipo> list)
        {
            List<MyShoeShop.Data.Models.Cipo> allShoes = this.logic.ReadAllCipo();
            foreach (MyShoeShop.Data.Models.Cipo item in allShoes)
            {
                Cipo newShoe = new Cipo()
                {
                    CipoId = decimal.ToInt32(item.CipoId),
                    Nev = item.Nev,
                    Meret = decimal.ToInt32(item.Meret),
                    Fajta = item.Fajta,
                    Szin = item.Szin,
                    Ar = decimal.ToInt32(item.Ar),
                    BeszallitoId = decimal.ToInt32(item.BeszallitoId),
                };
                list.Add(newShoe);
            }

            return list;
        }
    }
}
