﻿// <copyright file="IShoeLogic.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

namespace MyShoeShopGui.BL
{
    using System.Collections.Generic;
    using MyShoeShopGui.DATA;

    /// <summary>
    /// Interface for the Bl Shoelogic.
    /// </summary>
    internal interface IShoeLogic
    {
        /// <summary>
        /// Adding a shoe to the db.
        /// </summary>
        /// <param name="list">Temporary list for the whp.</param>
        void AddShoe(IList<Cipo> list);

        /// <summary>
        /// Modding one of the shoe int the temporary list of shoes.
        /// </summary>
        /// <param name="shoeToModify">The shoe which will be modified.</param>
        void ModShoe(Cipo shoeToModify);

        /// <summary>
        /// Deleting a shoe from the db.
        /// </summary>
        /// <param name="list">List of the shoes for the whp.</param>
        /// <param name="shoe">The shoe which will be deleted.</param>
        void DelShoe(IList<Cipo> list, Cipo shoe);

        /// <summary>
        /// Gets all the shoes from the db and gives them to the whp.
        /// </summary>
        /// <param name="list">The whp's list of shoes.</param>
        /// <returns>Returns the updated list to the whp.</returns>
        IList<Cipo> GetAllShoes(IList<Cipo> list);
    }
}
