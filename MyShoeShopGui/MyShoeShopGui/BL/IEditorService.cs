﻿// <copyright file="IEditorService.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

namespace MyShoeShopGui.BL
{
    using MyShoeShopGui.DATA;

    /// <summary>
    /// Interface for the editorwindow.
    /// </summary>
    internal interface IEditorService
    {
        /// <summary>
        /// Returns true if the shoe was edited.
        /// </summary>
        /// <param name="p">Shoe data type.</param>
        /// <returns>True if the shoe was edited.</returns>
        bool EditShoe(Cipo p);
    }
}
