﻿// <copyright file="Cipo.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

namespace MyShoeShopGui.DATA
{
    using System.Linq;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Shoe class for the whp data.
    /// </summary>
    public class Cipo : ObservableObject
    {
        private int cipoId;
        private string nev;
        private int meret;
        private string fajta;
        private string szin;
        private int ar;
        private int beszallitoId;

        /// <summary>
        /// Gets or sets the id of the shoe.
        /// </summary>
        public int CipoId
        {
            get { return this.cipoId; }
            set { this.Set(ref this.cipoId, value); }
        }

        /// <summary>
        /// Gets or sets the name of the shoe.
        /// </summary>
        public string Nev
        {
            get { return this.nev; }
            set { this.Set(ref this.nev, value); }
        }

        /// <summary>
        /// Gets or sets the size of the shoe.
        /// </summary>
        public int Meret
        {
            get { return this.meret; }
            set { this.Set(ref this.meret, value); }
        }

        /// <summary>
        /// Gets or sets the type of the shoe.
        /// </summary>
        public string Fajta
        {
            get { return this.fajta; }
            set { this.Set(ref this.fajta, value); }
        }

        /// <summary>
        /// Gets or sets the color of the shoe.
        /// </summary>
        public string Szin
        {
            get { return this.szin; }
            set { this.Set(ref this.szin, value); }
        }

        /// <summary>
        /// Gets or sets the price of the shoe.
        /// </summary>
        public int Ar
        {
            get { return this.ar; }
            set { this.Set(ref this.ar, value); }
        }

        /// <summary>
        /// Gets or sets shoes supplirs id.
        /// </summary>
        public int BeszallitoId
        {
            get { return this.beszallitoId; }
            set { this.Set(ref this.beszallitoId, value); }
        }

        /// <summary>
        /// Copies an other shoe.
        /// </summary>
        /// <param name="other">The other shoe.</param>
        public void CopyFrom(Cipo other)
        {
            this.GetType().GetProperties().ToList().ForEach(property => property.SetValue(this, property.GetValue(other)));
        }
    }
}
