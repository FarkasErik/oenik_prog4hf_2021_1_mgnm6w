// <copyright file="AssemblyInfo.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

using System.Windows;

[assembly: ThemeInfo(
    ResourceDictionaryLocation.None, // where theme specific resource dictionaries are located
    ResourceDictionaryLocation.SourceAssembly)

// (used if a resource is not found in the page,

// or application resource dictionaries)
// where the generic resource dictionary is located

// (used if a resource is not found in the page,
// app, or any theme specific resource dictionaries)
]
