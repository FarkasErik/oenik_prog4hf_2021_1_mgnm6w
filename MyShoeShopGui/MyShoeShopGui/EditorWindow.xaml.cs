﻿// <copyright file="EditorWindow.xaml.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

namespace MyShoeShopGui
{
    using System.Windows;
    using MyShoeShopGui.DATA;
    using MyShoeShopGui.VM;

    /// <summary>
    /// Interaction logic for EditorWindow.xaml.
    /// </summary>
    public partial class EditorWindow : Window
    {
        private readonly EditorViewModel vM;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorWindow"/> class.
        /// </summary>
        public EditorWindow()
        {
            this.InitializeComponent();
            this.vM = this.FindResource("VM") as EditorViewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorWindow"/> class.
        /// </summary>
        /// <param name="shoe">Shoe which is being edited.</param>
        public EditorWindow(Cipo shoe)
            : this()
        {
            this.vM.Cipo = shoe;
        }

        /// <summary>
        /// Gets the shoe which is being edited in the editor window.
        /// </summary>
        public Cipo Shoe
        {
            get { return this.vM.Cipo; }
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
