﻿// <copyright file="MyIOC.xaml.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

namespace MyShoeShopGui
{
    using System.Windows;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;
    using GalaSoft.MvvmLight.Messaging;
    using MyShoeShopGui.BL;
    using MyShoeShopGui.UI;

    /// <summary>
    /// Fix for simpleton.
    /// </summary>
    internal class MyIOC : SimpleIoc, IServiceLocator
    {
        /// <summary>
        /// Gets the instance.
        /// </summary>
        public static MyIOC Instance { get; private set; } = new MyIOC();
    }
}
