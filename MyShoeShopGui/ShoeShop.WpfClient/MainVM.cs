﻿// <copyright file="MainVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ShoeShop.WpfClient
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// View model for api.
    /// </summary>
    internal class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private ObservableCollection<ShoeVM> allShoes;
        private ShoeVM selectedShoe;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        /// <param name="logic">main logic.</param>
        public MainVM(MainLogic logic)
        {
            this.logic = logic;

            this.LoadCmd = new RelayCommand(() =>
                    this.AllShoes = new ObservableCollection<ShoeVM>(this.logic.ApiGetShoes()));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDelShoe(this.selectedShoe));
            this.AddCmd = new RelayCommand(() => this.logic.EditShoe(null, this.EditorFunc));
            this.ModCmd = new RelayCommand(() => this.logic.EditShoe(this.selectedShoe, this.EditorFunc));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<MainLogic>())
        {
        }

        /// <summary>
        /// Gets or sets the currently edited shoe.
        /// </summary>
        public ShoeVM SelectedShoe
        {
            get { return this.selectedShoe; }
            set { this.Set(ref this.selectedShoe, value); }
        }

        /// <summary>
        /// Gets or sets all the shoes.
        /// </summary>
        public ObservableCollection<ShoeVM> AllShoes
        {
            get { return this.allShoes; }
            set { this.Set(ref this.allShoes, value); }
        }

        /// <summary>
        /// Gets or sets editor function.
        /// </summary>
        public Func<ShoeVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets add command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets delete command.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets modify command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets load command.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}
