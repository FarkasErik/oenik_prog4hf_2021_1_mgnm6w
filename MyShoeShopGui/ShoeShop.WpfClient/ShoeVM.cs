﻿// <copyright file="ShoeVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ShoeShop.WpfClient
{
    using System.Linq;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// View Model for Api.
    /// </summary>
    public class ShoeVM : ObservableObject
    {
        private int shoeid;
        private string name;
        private int size;
        private string type;
        private string color;
        private int price;
        private int supplierid;

        /// <summary>
        /// Gets or sets the id of the shoe.
        /// </summary>
        public int Shoeid
        {
            get { return this.shoeid; }
            set { this.Set(ref this.shoeid, value); }
        }

        /// <summary>
        /// Gets or sets the name of the shoe.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets the size of the shoe.
        /// </summary>
        public int Size
        {
            get { return this.size; }
            set { this.Set(ref this.size, value); }
        }

        /// <summary>
        /// Gets or sets the type of the shoe.
        /// </summary>
        public string Type
        {
            get { return this.type; }
            set { this.Set(ref this.type, value); }
        }

        /// <summary>
        /// Gets or sets the color of the shoe.
        /// </summary>
        public string Color
        {
            get { return this.color; }
            set { this.Set(ref this.color, value); }
        }

        /// <summary>
        /// Gets or sets the price of the shoe.
        /// </summary>
        public int Price
        {
            get { return this.price; }
            set { this.Set(ref this.price, value); }
        }

        /// <summary>
        /// Gets or sets shoes supplirs id.
        /// </summary>
        public int Supplierid
        {
            get { return this.supplierid; }
            set { this.Set(ref this.supplierid, value); }
        }

        /// <summary>
        /// Copies an other shoe.
        /// </summary>
        /// <param name="other">The other shoe.</param>
        public void CopyFrom(ShoeVM other)
        {
            this.GetType().GetProperties().ToList().ForEach(property => property.SetValue(this, property.GetValue(other)));
        }
    }
}
