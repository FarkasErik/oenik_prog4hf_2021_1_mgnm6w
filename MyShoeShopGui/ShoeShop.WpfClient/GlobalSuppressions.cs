﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Diagnostics.CodeAnalysis;

[assembly: CLSCompliant(false)]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1128:Put constructor initializers on their own line", Justification = "idk", Scope = "member", Target = "~M:ShoeShop.WpfClient.EditorWindow.#ctor(ShoeShop.WpfClient.ShoeVM)")]
[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "idk", Scope = "type", Target = "~T:ShoeShop.WpfClient.MainLogic")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "idk", Scope = "member", Target = "~M:ShoeShop.WpfClient.MainLogic.ApiGetShoes~System.Collections.Generic.List{ShoeShop.WpfClient.ShoeVM}")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "idk", Scope = "member", Target = "~M:ShoeShop.WpfClient.MainLogic.ApiDelShoe(ShoeShop.WpfClient.ShoeVM)")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "idk", Scope = "member", Target = "~M:ShoeShop.WpfClient.MainLogic.ApiDelShoe(ShoeShop.WpfClient.ShoeVM)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "idk", Scope = "member", Target = "~M:ShoeShop.WpfClient.MainLogic.ApiEditShoe(ShoeShop.WpfClient.ShoeVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "idk", Scope = "member", Target = "~M:ShoeShop.WpfClient.MainLogic.ApiEditShoe(ShoeShop.WpfClient.ShoeVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "idk", Scope = "member", Target = "~M:ShoeShop.WpfClient.MainLogic.ApiEditShoe(ShoeShop.WpfClient.ShoeVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1202:Elements should be ordered by access", Justification = "idk", Scope = "member", Target = "~M:ShoeShop.WpfClient.MainLogic.EditShoe(ShoeShop.WpfClient.ShoeVM,System.Func{ShoeShop.WpfClient.ShoeVM,System.Boolean})")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1202:Elements should be ordered by access", Justification = "idk", Scope = "member", Target = "~M:ShoeShop.WpfClient.MainLogic.ApiDelShoe(ShoeShop.WpfClient.ShoeVM)")]
[assembly: SuppressMessage("Design", "CA1812:Avoid uninstantiated internal classes", Justification = "False error.")]