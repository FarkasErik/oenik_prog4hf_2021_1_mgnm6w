﻿// <copyright file="MyIOC.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ShoeShop.WpfClient
{
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;

    /// <summary>
    /// MyIoc.
    /// </summary>
    internal class MyIOC : SimpleIoc, IServiceLocator
    {
        /// <summary>
        /// Gets the instance.
        /// </summary>
        public static MyIOC Instance { get; private set; } = new MyIOC();
    }
}
