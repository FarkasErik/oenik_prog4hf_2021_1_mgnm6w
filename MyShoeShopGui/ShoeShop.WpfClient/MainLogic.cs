﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ShoeShop.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text.Json;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Main logic for Api.
    /// </summary>
    internal class MainLogic : ViewModelBase
    {
        private string url = "https://localhost:44324/ShoesApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <summary>
        /// Gets the shoe.
        /// </summary>
        /// <returns>The list of shoes.</returns>
        public List<ShoeVM> ApiGetShoes()
        {
            string json = this.client.GetStringAsync(this.url + "all").Result;
            var list = JsonSerializer.Deserialize<List<ShoeVM>>(json, this.jsonOptions);
            return list;
        }

        private static void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "ShoeResult");
        }

        /// <summary>
        /// Delete one shoe.
        /// </summary>
        /// <param name="shoe">The shoe which needs to be deleted.</param>
        public void ApiDelShoe(ShoeVM shoe)
        {
            bool success = false;
            if (shoe != null)
            {
                string json = this.client.GetStringAsync(this.url + "del/" + shoe.Shoeid.ToString()).Result;
                JsonDocument doc = JsonDocument.Parse(json);

                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            SendMessage(success);
        }

        private bool ApiEditShoe(ShoeVM shoe, bool isEditing)
        {
            if (shoe == null)
            {
                return false;
            }

            string myUrl = isEditing ? this.url + "mod" : this.url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            postData.Add("Shoeid", shoe.Shoeid.ToString());

            postData.Add("Name", shoe.Name);
            postData.Add("Size", shoe.Size.ToString());
            postData.Add("Type", shoe.Type.ToString());
            postData.Add("Color", shoe.Color.ToString());
            postData.Add("Price", shoe.Price.ToString());
            postData.Add("Supplierid", shoe.Supplierid.ToString());
            string json = this.client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).
                Result.Content.ReadAsStringAsync().Result;

            JsonDocument doc = JsonDocument.Parse(json);
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }

        /// <summary>
        /// Edit or add a shoe.
        /// </summary>
        /// <param name="shoe">The shoe which is edited or added.</param>
        /// <param name="editor">Whether the shoe need to be edited.</param>
        public void EditShoe(ShoeVM shoe, Func<ShoeVM, bool> editor)
        {
            ShoeVM clone = new ShoeVM();
            if (shoe != null)
            {
                clone.CopyFrom(shoe);
            }

            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (shoe != null)
                {
                    success = this.ApiEditShoe(clone, true);
                }
                else
                {
                    success = this.ApiEditShoe(clone, false);
                }
            }

            SendMessage(success == true);
        }
    }
}
