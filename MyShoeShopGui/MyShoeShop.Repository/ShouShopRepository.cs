﻿// <copyright file="ShouShopRepository.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

namespace MyShoeShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using MyShoeShop.Data.Models;

    /// <summary>
    /// Main SHoe Shop repository implementing the Crud and other inquirys.
    /// </summary>
    public class ShouShopRepository : IShoeShopRepository
    {
        private ShoeDatabaseContext db;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShouShopRepository"/> class.
        /// </summary>
        /// <param name="db"> ShoeShop database. </param>
        public ShouShopRepository(ShoeDatabaseContext db)
        {
            this.db = db;
        }

        /// <inheritdoc/>
        public void Create(Cipo shoe)
        {
            this.db.Cipos.Add(shoe);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Create(Beszallito beszallito)
        {
            this.db.Beszallitos.Add(beszallito);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Create(Vasarlo vasarlo)
        {
            this.db.Vasarlos.Add(vasarlo);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public List<Cipo> ReadAllCipo()
        {
            return this.db.Cipos.ToList();
        }

        /// <inheritdoc/>
        public List<Beszallito> ReadAllBeszallito()
        {
            return this.db.Beszallitos.ToList();
        }

        /// <inheritdoc/>
        public List<Vasarlo> ReadAllVasarlo()
        {
            return this.db.Vasarlos.ToList();
        }

        /// <inheritdoc/>
        public void UpdateCipo(int cipoID, Cipo cipo)
        {
            List<Cipo> cipok = this.db.Cipos.ToList();
            var regiCipo = cipok.Where(x => x.CipoId == cipoID).FirstOrDefault();
            regiCipo.CipoId = cipo.CipoId;
            regiCipo.Nev = cipo.Nev;
            regiCipo.Ar = cipo.Ar;
            regiCipo.BeszallitoId = cipo.BeszallitoId;
            regiCipo.Fajta = cipo.Fajta;
            regiCipo.Meret = cipo.Meret;
            regiCipo.Szin = cipo.Szin;

            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateVasarloCipo(int cipoID, int ujCipoID)
        {
            List<Vasarlo> vasarlok = this.db.Vasarlos.ToList();
            vasarlok.Where(x => x.CipoId == cipoID).FirstOrDefault().CipoId = ujCipoID;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateBeszallitoNepszeruseg(int beszallitoID, string ujNepszeruseg)
        {
            List<Beszallito> beszallitok = this.db.Beszallitos.ToList();
            beszallitok.Where(x => x.BeszallitoId == beszallitoID).FirstOrDefault().Nepszeruseg = ujNepszeruseg;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void DeleteCipo(int cipoID)
        {
            List<Cipo> cipok = this.db.Cipos.ToList();
            this.db.Cipos.Remove(cipok.Where(x => x.CipoId == cipoID).FirstOrDefault());
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void DeleteBeszallito(int beszallitoID)
        {
            List<Beszallito> beszallitok = this.db.Beszallitos.ToList();
            this.db.Beszallitos.Remove(beszallitok.Where(x => x.BeszallitoId == beszallitoID).FirstOrDefault());
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void DeleteVasarlo(string email)
        {
            List<Vasarlo> vasarlok = this.db.Vasarlos.ToList();
            this.db.Vasarlos.Remove(vasarlok.Where(x => x.Email.Equals(email, StringComparison.Ordinal)).FirstOrDefault());
            this.db.SaveChanges();
        }
    }
}
