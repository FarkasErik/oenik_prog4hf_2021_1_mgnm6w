﻿// <copyright file="IShoeShopRepository.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

namespace MyShoeShop.Repository
{
    using System.Collections.Generic;
    using MyShoeShop.Data.Models;

    /// <summary>
    /// Interface for declaring the mandatory methods for the main repo.
    /// </summary>
    public interface IShoeShopRepository : IRepository<Cipo>
    {
        // CRUD

        /// <summary>
        /// Adding shoe to the db.
        /// </summary>
        /// <param name="shoe"> Adds shoe to the db. </param>
        void Create(Cipo shoe);

        /// <summary>
        /// Adding a new Supplier to the db.
        /// </summary>
        /// <param name="beszallito"> Adds the neww supplier. </param>
        void Create(Beszallito beszallito);

        /// <summary>
        /// Adding a new customer to the db.
        /// </summary>
        /// <param name="vasarlo"> Adds the new customer to the db. </param>
        void Create(Vasarlo vasarlo);

        /// <summary>
        /// Returns the suppliers from the db to the logic.
        /// </summary>
        /// <returns> The list of supplier. </returns>
        List<Beszallito> ReadAllBeszallito();

        /// <summary>
        /// Returns the customers to the logic.
        /// </summary>
        /// <returns> The list of customers. </returns>
        List<Vasarlo> ReadAllVasarlo();

        /// <summary>
        /// Updates the name of a shoe.
        /// </summary>
        /// <param name="cipoID"> ID of the shoe which is to be updated. </param>
        /// <param name="cipo"> The new shoe. </param>
        void UpdateCipo(int cipoID, Cipo cipo);

        /// <summary>
        /// Updates which shoe the customer wants to buy.
        /// </summary>
        /// <param name="cipoID"> Id of the shoe which is to be replaced. </param>
        /// <param name="ujCipoID"> ID of the new shoe. </param>
        void UpdateVasarloCipo(int cipoID, int ujCipoID);

        /// <summary>
        /// Updates the popularity of a supplier.
        /// </summary>
        /// <param name="beszallitoID"> ID of the supplier. </param>
        /// <param name="ujNepszeruseg"> The Value of the supplliers new popularity. </param>
        void UpdateBeszallitoNepszeruseg(int beszallitoID, string ujNepszeruseg);

        /// <summary>
        /// Deletes a shoe.
        /// </summary>
        /// <param name="cipoID"> ID of the shoe. </param>
        void DeleteCipo(int cipoID);

        /// <summary>
        /// Deletes a supplier.
        /// </summary>
        /// <param name="beszallitoID"> ID of the supplier. </param>
        void DeleteBeszallito(int beszallitoID);

        /// <summary>
        /// Deletes a custoer.
        /// </summary>
        /// <param name="email"> Email of the customer. </param>
        void DeleteVasarlo(string email);
    }
}
