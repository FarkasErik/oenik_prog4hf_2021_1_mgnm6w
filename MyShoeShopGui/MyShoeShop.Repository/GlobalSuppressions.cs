﻿// <copyright file="GlobalSuppressions.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

using System;
using System.Diagnostics.CodeAnalysis;

[assembly: CLSCompliant(false)]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "IDK", Scope = "member", Target = "~M:MyShoeShop.Repository.IShoeShopRepository.ReadAllVasarlo~System.Collections.Generic.List{MyShoeShop.Data.Models.Vasarlo}")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "IDK", Scope = "member", Target = "~M:MyShoeShop.Repository.IShoeShopRepository.ReadAllBeszallito~System.Collections.Generic.List{MyShoeShop.Data.Models.Beszallito}")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "IDK", Scope = "member", Target = "~M:MyShoeShop.Repository.IRepository`1.ReadAllCipo~System.Collections.Generic.List{MyShoeShop.Data.Models.Cipo}")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "idk", Scope = "member", Target = "~M:MyShoeShop.Repository.ShouShopRepository.UpdateCipo(System.Int32,MyShoeShop.Data.Models.Cipo)")]
