﻿// <copyright file="IRepository{T}.cs" company="MGNM6W">
// Copyright (c) MGNM6W. All rights reserved.
// </copyright>

namespace MyShoeShop.Repository
{
    using System.Collections.Generic;
    using MyShoeShop.Data.Models;

    /// <summary>
    /// IRepository interface.
    /// </summary>
    /// <typeparam name="T"> generic T. </typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// ReadAll Shoe.
        /// </summary>
        /// <returns>Return the lis of the shoes.</returns>
        List<Cipo> ReadAllCipo();
    }
}
