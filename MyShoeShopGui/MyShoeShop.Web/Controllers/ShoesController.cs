﻿// <copyright file="ShoesController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyShoeShop.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;
    using MyShoeShop.Logic;
    using MyShoeShop.Web.Models;

    /// <summary>
    /// ShoesController.
    /// </summary>
    public class ShoesController : Controller
    {
        private ShoeLogic logic;
        private IMapper mapper;
        private ShoeListViewModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShoesController"/> class.
        /// </summary>
        /// <param name="logic">ShoeLogic.</param>
        /// <param name="mapper">IMapper.</param>
        public ShoesController(ShoeLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;

            this.model = new ShoeListViewModel();
            this.model.EditedShoe = new Models.Shoe();

            var shoe = logic.ReadAllCipo();
            this.model.Shoes = mapper.Map<IList<Data.Models.Cipo>, List<Models.Shoe>>(shoe);
        }

        /// <summary>
        /// Index.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("ShoesIndex", this.model);
        }

        /// <summary>
        /// Details.
        /// </summary>
        /// <param name="id">int.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult Details(int id)
        {
            return this.View("ShoesDetails", this.GetShoeModel(id));
        }

        /// <summary>
        /// Remove.
        /// </summary>
        /// <param name="id">int.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult Remove(int id)
        {
            this.TempData["editResult"] = "Delete FAIL";
            this.logic.DeleteCipo(id);
            this.TempData["editResult"] = "Delete OK";
            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Edit.
        /// </summary>
        /// <param name="id">int.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult Edit(int id)
        {
            this.ViewData["editAction"] = "Edit";
            this.model.EditedShoe = this.GetShoeModel(id);
            return this.View("ShoesIndex", this.model);
        }

        /// <summary>
        /// Edit.
        /// </summary>
        /// <param name="shoe">Shoe.</param>
        /// <param name="editAction">string.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult Edit(Models.Shoe shoe, string editAction)
        {
            if (this.ModelState.IsValid && shoe != null)
            {
                this.TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    try
                    {
                        Data.Models.Cipo cipo = new Data.Models.Cipo() { CipoId = shoe.Shoeid, Nev = shoe.Name, Meret = shoe.Size, Fajta = shoe.Type, Szin = shoe.Color, Ar = shoe.Price, BeszallitoId = shoe.Supplierid };
                        this.logic.CreateCipo(cipo);
                    }
                    catch (ArgumentException ex)
                    {
                        this.TempData["editResult"] = "Insert FAIL: " + ex.Message;
                    }
                }
                else
                {
                    Data.Models.Cipo cipo = new Data.Models.Cipo() { CipoId = shoe.Shoeid, Nev = shoe.Name, Meret = shoe.Size, Fajta = shoe.Type, Szin = shoe.Color, Ar = shoe.Price, BeszallitoId = shoe.Supplierid };
                    this.logic.UpdateCipo(shoe.Shoeid, cipo);
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.ViewData["editAction"] = "Edit";
                this.model.EditedShoe = shoe;
                return this.View("ShoesIndex", this.model);
            }
        }

        private Models.Shoe GetShoeModel(int id)
        {
            Data.Models.Cipo oneShoe = this.logic.ReadCipo(id);
            return this.mapper.Map<Data.Models.Cipo, Models.Shoe>(oneShoe);
        }
    }
}
