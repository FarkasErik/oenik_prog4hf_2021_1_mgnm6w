﻿// <copyright file="ShoesApiController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyShoeShop.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;
    using MyShoeShop.Logic;

    /// <summary>
    /// Controller for api.
    /// </summary>
    public class ShoesApiController : Controller
    {
        private ShoeLogic logic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShoesApiController"/> class.
        /// </summary>
        /// <param name="logic">sets the logic.</param>
        /// <param name="mapper">set the mapper.</param>
        public ShoesApiController(ShoeLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        /// <summary>
        /// Gets all the shoes from the db.
        /// </summary>
        /// <returns>the shoes.</returns>
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Shoe> GetAll()
        {
            var shoes = this.logic.ReadAllCipo();
            return this.mapper.Map<IList<Data.Models.Cipo>, List<Models.Shoe>>(shoes);
        }

        /// <summary>
        /// Deletes on shoe.
        /// </summary>
        /// <param name="id">The id of the to be deleted shoe.</param>
        /// <returns>Returns whether the operation was succesful.</returns>
        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneShoe(int id)
        {
            try
            {
                this.logic.DeleteCipo(id);
                return new ApiResult() { OperationResult = true };
            }
            catch (ArgumentException)
            {
                return new ApiResult() { OperationResult = false };
            }
        }

        /// <summary>
        /// Add one shoe.
        /// </summary>
        /// <param name="shoe">The shoe which is to be added to the db.</param>
        /// <returns>Returns whether the operation was succesful.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneShoe(Models.Shoe shoe)
        {
            bool success = true;
            try
            {
                Data.Models.Cipo cipo = new Data.Models.Cipo() { CipoId = shoe.Shoeid, Nev = shoe.Name, Meret = shoe.Size, Fajta = shoe.Type, Szin = shoe.Color, Ar = shoe.Price, BeszallitoId = shoe.Supplierid };
                this.logic.CreateCipo(cipo);
            }
            catch (ArgumentException)
            {
                success = false;
            }

            return new ApiResult() { OperationResult = success };
        }

        /// <summary>
        /// Modifies a shoe.
        /// </summary>
        /// <param name="shoe">The shoe which is to be modified.</param>
        /// <returns>Returns whether the operation was succesful.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneShoe(Models.Shoe shoe)
        {
            try
            {
                Data.Models.Cipo cipo = new Data.Models.Cipo() { CipoId = shoe.Shoeid, Nev = shoe.Name, Meret = shoe.Size, Fajta = shoe.Type, Szin = shoe.Color, Ar = shoe.Price, BeszallitoId = shoe.Supplierid };
                this.logic.UpdateCipo(shoe.Shoeid, cipo);
                return new ApiResult() { OperationResult = true };
            }
            catch (ArgumentException)
            {
                return new ApiResult() { OperationResult = false };
            }
        }
    }
}
