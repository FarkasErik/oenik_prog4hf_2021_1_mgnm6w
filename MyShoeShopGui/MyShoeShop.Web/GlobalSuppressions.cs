﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Diagnostics.CodeAnalysis;

[assembly: CLSCompliant(false)]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "idk", Scope = "member", Target = "~M:MyShoeShop.Web.Controllers.ShoesController.#ctor(MyShoeShop.Logic.ShoeLogic,AutoMapper.IMapper)")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "idk", Scope = "member", Target = "~P:MyShoeShop.Web.Models.ShoeListViewModel.Shoes")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "idk", Scope = "member", Target = "~P:MyShoeShop.Web.Models.ShoeListViewModel.Shoes")]
[assembly: SuppressMessage("Style", "IDE0059:Unnecessary assignment of a value", Justification = "idk", Scope = "member", Target = "~M:MyShoeShop.Web.Controllers.ShoesApiController.DelOneShoe(System.Int32)~MyShoeShop.Web.Controllers.ShoesApiController.ApiResult")]
[assembly: SuppressMessage("Style", "IDE0059:Unnecessary assignment of a value", Justification = "idk", Scope = "member", Target = "~M:MyShoeShop.Web.Controllers.ShoesApiController.DelOneShoe(System.Int32)~MyShoeShop.Web.ApiResult")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "idk", Scope = "member", Target = "~M:MyShoeShop.Web.Controllers.ShoesApiController.AddOneShoe(MyShoeShop.Web.Models.Shoe)~MyShoeShop.Web.ApiResult")]
[assembly: SuppressMessage("Style", "IDE0059:Unnecessary assignment of a value", Justification = "idk", Scope = "member", Target = "~M:MyShoeShop.Web.Controllers.ShoesApiController.AddOneShoe(MyShoeShop.Web.Models.Shoe)~MyShoeShop.Web.ApiResult")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "idk", Scope = "member", Target = "~M:MyShoeShop.Web.Controllers.ShoesApiController.ModOneShoe(MyShoeShop.Web.Models.Shoe)~MyShoeShop.Web.ApiResult")]
[assembly: SuppressMessage("Style", "IDE0059:Unnecessary assignment of a value", Justification = "idk", Scope = "member", Target = "~M:MyShoeShop.Web.Controllers.ShoesApiController.ModOneShoe(MyShoeShop.Web.Models.Shoe)~MyShoeShop.Web.ApiResult")]
