﻿// <copyright file="ShoeListViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyShoeShop.Web.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// ShoeListViewModel.
    /// </summary>
    public class ShoeListViewModel
    {
        /// <summary>
        /// Gets or sets shoes.
        /// </summary>
        public List<Shoe> Shoes { get; set; }

        /// <summary>
        /// Gets or sets editedshoe.
        /// </summary>
        public Shoe EditedShoe { get; set; }
    }
}
