﻿// <copyright file="Shoe.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyShoeShop.Web.Models
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// shoe.
    /// </summary>
    public class Shoe
    {
        /// <summary>
        /// Gets or sets shoe_id.
        /// </summary>
        [Display(Name = "Shoe id")]
        [Required]
        public int Shoeid { get; set; }

        /// <summary>
        /// Gets or sets name.
        /// </summary>
        [Display(Name = "Name")]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets size.
        /// </summary>
        [Display(Name = "Size")]
        [Required]
        public int Size { get; set; }

        /// <summary>
        /// Gets or sets type.
        /// </summary>
        [Display(Name = "Type")]
        [Required]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets color.
        /// </summary>
        [Display(Name = "Color")]
        [Required]
        public string Color { get; set; }

        /// <summary>
        /// Gets or sets price.
        /// </summary>
        [Display(Name = "Price")]
        public int Price { get; set; }

        /// <summary>
        /// Gets or sets supplier_id.
        /// </summary>
        [Display(Name = "Supplier id")]
        [Required]
        public int Supplierid { get; set; }
    }
}
