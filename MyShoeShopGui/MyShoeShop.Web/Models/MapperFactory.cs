﻿// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyShoeShop.Web.Models
{
    using AutoMapper;

    /// <summary>
    /// MapperFactory.
    /// </summary>
    public static class MapperFactory
    {
        /// <summary>
        /// CreateMapper.
        /// </summary>
        /// <returns>IMapper.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MyShoeShop.Data.Models.Cipo, MyShoeShop.Web.Models.Shoe>().
                    ForMember(dest => dest.Shoeid, map => map.MapFrom(src => src.CipoId)).
                    ForMember(dest => dest.Name, map => map.MapFrom(src => src.Nev)).
                    ForMember(dest => dest.Size, map => map.MapFrom(src => src.Meret)).
                    ForMember(dest => dest.Price, map => map.MapFrom(src => src.Ar)).
                    ForMember(dest => dest.Color, map => map.MapFrom(src => src.Szin)).
                    ForMember(dest => dest.Type, map => map.MapFrom(src => src.Fajta)).
                    ForMember(dest => dest.Supplierid, map => map.MapFrom(src => src.BeszallitoId));
            });
            return config.CreateMapper();
        }
    }
}
