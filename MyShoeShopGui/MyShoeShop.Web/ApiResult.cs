﻿// <copyright file="ApiResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyShoeShop.Web
{
    /// <summary>
    /// Contains a bool wether the operation was succesful.
    /// </summary>
    public class ApiResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether the Operation was succesful.
        /// </summary>
        public bool OperationResult { get; set; }
    }
}
