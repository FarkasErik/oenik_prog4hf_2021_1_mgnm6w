var namespace_my_shoe_shop_1_1_web_1_1_models =
[
    [ "ErrorViewModel", "class_my_shoe_shop_1_1_web_1_1_models_1_1_error_view_model.html", "class_my_shoe_shop_1_1_web_1_1_models_1_1_error_view_model" ],
    [ "MapperFactory", "class_my_shoe_shop_1_1_web_1_1_models_1_1_mapper_factory.html", "class_my_shoe_shop_1_1_web_1_1_models_1_1_mapper_factory" ],
    [ "Shoe", "class_my_shoe_shop_1_1_web_1_1_models_1_1_shoe.html", "class_my_shoe_shop_1_1_web_1_1_models_1_1_shoe" ],
    [ "ShoeListViewModel", "class_my_shoe_shop_1_1_web_1_1_models_1_1_shoe_list_view_model.html", "class_my_shoe_shop_1_1_web_1_1_models_1_1_shoe_list_view_model" ]
];