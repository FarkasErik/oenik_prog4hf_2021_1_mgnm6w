var searchData=
[
  ['bl_170',['BL',['../namespace_my_shoe_shop_gui_1_1_b_l.html',1,'MyShoeShopGui']]],
  ['controllers_171',['Controllers',['../namespace_my_shoe_shop_1_1_web_1_1_controllers.html',1,'MyShoeShop::Web']]],
  ['data_172',['Data',['../namespace_my_shoe_shop_1_1_data.html',1,'MyShoeShop']]],
  ['data_173',['DATA',['../namespace_my_shoe_shop_gui_1_1_d_a_t_a.html',1,'MyShoeShopGui']]],
  ['logic_174',['Logic',['../namespace_my_shoe_shop_1_1_logic.html',1,'MyShoeShop']]],
  ['models_175',['Models',['../namespace_my_shoe_shop_1_1_data_1_1_models.html',1,'MyShoeShop.Data.Models'],['../namespace_my_shoe_shop_1_1_web_1_1_models.html',1,'MyShoeShop.Web.Models']]],
  ['myshoeshop_176',['MyShoeShop',['../namespace_my_shoe_shop.html',1,'']]],
  ['myshoeshopgui_177',['MyShoeShopGui',['../namespace_my_shoe_shop_gui.html',1,'']]],
  ['repository_178',['Repository',['../namespace_my_shoe_shop_1_1_repository.html',1,'MyShoeShop']]],
  ['ui_179',['UI',['../namespace_my_shoe_shop_gui_1_1_u_i.html',1,'MyShoeShopGui']]],
  ['vm_180',['VM',['../namespace_my_shoe_shop_gui_1_1_v_m.html',1,'MyShoeShopGui']]],
  ['web_181',['Web',['../namespace_my_shoe_shop_1_1_web.html',1,'MyShoeShop']]]
];
