var searchData=
[
  ['cipo_243',['Cipo',['../class_my_shoe_shop_1_1_data_1_1_models_1_1_vasarlo.html#afebea6b4ae847a92b5461d3654be143b',1,'MyShoeShop.Data.Models.Vasarlo.Cipo()'],['../class_my_shoe_shop_gui_1_1_v_m_1_1_editor_view_model.html#ad0fc7da46efbb3c674bf4b08f5bb24a8',1,'MyShoeShopGui.VM.EditorViewModel.Cipo()']]],
  ['cipoid_244',['CipoId',['../class_my_shoe_shop_1_1_data_1_1_models_1_1_cipo.html#ac523cd0b968542ac176a63e8adeccc8c',1,'MyShoeShop.Data.Models.Cipo.CipoId()'],['../class_my_shoe_shop_1_1_data_1_1_models_1_1_vasarlo.html#a3b1eb31b1bceb5cccd646a335eefe942',1,'MyShoeShop.Data.Models.Vasarlo.CipoId()'],['../class_my_shoe_shop_gui_1_1_d_a_t_a_1_1_cipo.html#af3e81f4380eac7053a58d64fe98b2268',1,'MyShoeShopGui.DATA.Cipo.CipoId()']]],
  ['cipos_245',['Cipos',['../class_my_shoe_shop_1_1_data_1_1_models_1_1_beszallito.html#a5e92a1907cacb01d2a01f5632596398a',1,'MyShoeShop.Data.Models.Beszallito.Cipos()'],['../class_my_shoe_shop_1_1_data_1_1_models_1_1_shoe_database_context.html#ac844486ec30f606245341793a81eafef',1,'MyShoeShop.Data.Models.ShoeDatabaseContext.Cipos()']]],
  ['color_246',['Color',['../class_my_shoe_shop_1_1_web_1_1_models_1_1_shoe.html#a262ba12544e01e50554b01e53e2dc35e',1,'MyShoeShop::Web::Models::Shoe']]],
  ['configuration_247',['Configuration',['../class_my_shoe_shop_1_1_web_1_1_startup.html#a1e97b79cada303cc35b19419588315d9',1,'MyShoeShop::Web::Startup']]]
];
