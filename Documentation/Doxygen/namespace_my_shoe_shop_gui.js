var namespace_my_shoe_shop_gui =
[
    [ "BL", "namespace_my_shoe_shop_gui_1_1_b_l.html", "namespace_my_shoe_shop_gui_1_1_b_l" ],
    [ "DATA", "namespace_my_shoe_shop_gui_1_1_d_a_t_a.html", "namespace_my_shoe_shop_gui_1_1_d_a_t_a" ],
    [ "UI", "namespace_my_shoe_shop_gui_1_1_u_i.html", "namespace_my_shoe_shop_gui_1_1_u_i" ],
    [ "VM", "namespace_my_shoe_shop_gui_1_1_v_m.html", "namespace_my_shoe_shop_gui_1_1_v_m" ],
    [ "App", "class_my_shoe_shop_gui_1_1_app.html", "class_my_shoe_shop_gui_1_1_app" ],
    [ "EditorWindow", "class_my_shoe_shop_gui_1_1_editor_window.html", "class_my_shoe_shop_gui_1_1_editor_window" ],
    [ "MainWindow", "class_my_shoe_shop_gui_1_1_main_window.html", "class_my_shoe_shop_gui_1_1_main_window" ],
    [ "MyIOC", "class_my_shoe_shop_gui_1_1_my_i_o_c.html", "class_my_shoe_shop_gui_1_1_my_i_o_c" ]
];