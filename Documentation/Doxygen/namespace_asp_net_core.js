var namespace_asp_net_core =
[
    [ "Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", "class_asp_net_core_1_1_views_____view_imports" ],
    [ "Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", "class_asp_net_core_1_1_views_____view_start" ],
    [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
    [ "Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", "class_asp_net_core_1_1_views___home___privacy" ],
    [ "Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", "class_asp_net_core_1_1_views___shared_____layout" ],
    [ "Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial" ],
    [ "Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", "class_asp_net_core_1_1_views___shared___error" ],
    [ "Views_Shoes_ShoesDetails", "class_asp_net_core_1_1_views___shoes___shoes_details.html", "class_asp_net_core_1_1_views___shoes___shoes_details" ],
    [ "Views_Shoes_ShoesEdit", "class_asp_net_core_1_1_views___shoes___shoes_edit.html", "class_asp_net_core_1_1_views___shoes___shoes_edit" ],
    [ "Views_Shoes_ShoesIndex", "class_asp_net_core_1_1_views___shoes___shoes_index.html", "class_asp_net_core_1_1_views___shoes___shoes_index" ],
    [ "Views_Shoes_ShoesList", "class_asp_net_core_1_1_views___shoes___shoes_list.html", "class_asp_net_core_1_1_views___shoes___shoes_list" ]
];