var interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository =
[
    [ "Create", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#a577f1656a6ee3f33f46c7e34c400fa0e", null ],
    [ "Create", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#acd6cd14999ce1a9df6ce916ea586a095", null ],
    [ "Create", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#a1b98f9a7e15650b0459e599efefc0a74", null ],
    [ "DeleteBeszallito", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#aeaf8680297df118959445d30de47aeb4", null ],
    [ "DeleteCipo", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#a64a14f44053e1f82ad616d5cf533e1af", null ],
    [ "DeleteVasarlo", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#a176300215503c49452a469356a8400c4", null ],
    [ "ReadAllBeszallito", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#a078fb0b2a19dd6f3cf3d5941765a04ce", null ],
    [ "ReadAllVasarlo", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#a5881c0eea619f748b09ae9ac23a474f4", null ],
    [ "UpdateBeszallitoNepszeruseg", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#a4d1de380b29c68ddccd81871a44db7db", null ],
    [ "UpdateCipo", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#a58585568524ffbef9899b5d151daf1f5", null ],
    [ "UpdateVasarloCipo", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#a5467d8890f313ee0ba5e0f92261cd10c", null ]
];