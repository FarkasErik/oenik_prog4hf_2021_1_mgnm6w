var annotated_dup =
[
    [ "AspNetCore", "namespace_asp_net_core.html", [
      [ "Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", "class_asp_net_core_1_1_views_____view_imports" ],
      [ "Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", "class_asp_net_core_1_1_views_____view_start" ],
      [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
      [ "Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", "class_asp_net_core_1_1_views___home___privacy" ],
      [ "Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", "class_asp_net_core_1_1_views___shared_____layout" ],
      [ "Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial" ],
      [ "Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", "class_asp_net_core_1_1_views___shared___error" ],
      [ "Views_Shoes_ShoesDetails", "class_asp_net_core_1_1_views___shoes___shoes_details.html", "class_asp_net_core_1_1_views___shoes___shoes_details" ],
      [ "Views_Shoes_ShoesEdit", "class_asp_net_core_1_1_views___shoes___shoes_edit.html", "class_asp_net_core_1_1_views___shoes___shoes_edit" ],
      [ "Views_Shoes_ShoesIndex", "class_asp_net_core_1_1_views___shoes___shoes_index.html", "class_asp_net_core_1_1_views___shoes___shoes_index" ],
      [ "Views_Shoes_ShoesList", "class_asp_net_core_1_1_views___shoes___shoes_list.html", "class_asp_net_core_1_1_views___shoes___shoes_list" ]
    ] ],
    [ "MyShoeShop", "namespace_my_shoe_shop.html", [
      [ "Data", "namespace_my_shoe_shop_1_1_data.html", [
        [ "Models", "namespace_my_shoe_shop_1_1_data_1_1_models.html", [
          [ "Beszallito", "class_my_shoe_shop_1_1_data_1_1_models_1_1_beszallito.html", "class_my_shoe_shop_1_1_data_1_1_models_1_1_beszallito" ],
          [ "Cipo", "class_my_shoe_shop_1_1_data_1_1_models_1_1_cipo.html", "class_my_shoe_shop_1_1_data_1_1_models_1_1_cipo" ],
          [ "ShoeDatabaseContext", "class_my_shoe_shop_1_1_data_1_1_models_1_1_shoe_database_context.html", "class_my_shoe_shop_1_1_data_1_1_models_1_1_shoe_database_context" ],
          [ "Vasarlo", "class_my_shoe_shop_1_1_data_1_1_models_1_1_vasarlo.html", "class_my_shoe_shop_1_1_data_1_1_models_1_1_vasarlo" ]
        ] ]
      ] ],
      [ "Logic", "namespace_my_shoe_shop_1_1_logic.html", [
        [ "IShoeLogic", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic" ],
        [ "ShoeLogic", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic" ]
      ] ],
      [ "Repository", "namespace_my_shoe_shop_1_1_repository.html", [
        [ "IRepository", "interface_my_shoe_shop_1_1_repository_1_1_i_repository.html", "interface_my_shoe_shop_1_1_repository_1_1_i_repository" ],
        [ "IShoeShopRepository", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository" ],
        [ "ShouShopRepository", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository" ]
      ] ],
      [ "Web", "namespace_my_shoe_shop_1_1_web.html", [
        [ "Controllers", "namespace_my_shoe_shop_1_1_web_1_1_controllers.html", [
          [ "HomeController", "class_my_shoe_shop_1_1_web_1_1_controllers_1_1_home_controller.html", "class_my_shoe_shop_1_1_web_1_1_controllers_1_1_home_controller" ],
          [ "ShoesController", "class_my_shoe_shop_1_1_web_1_1_controllers_1_1_shoes_controller.html", "class_my_shoe_shop_1_1_web_1_1_controllers_1_1_shoes_controller" ]
        ] ],
        [ "Models", "namespace_my_shoe_shop_1_1_web_1_1_models.html", [
          [ "ErrorViewModel", "class_my_shoe_shop_1_1_web_1_1_models_1_1_error_view_model.html", "class_my_shoe_shop_1_1_web_1_1_models_1_1_error_view_model" ],
          [ "MapperFactory", "class_my_shoe_shop_1_1_web_1_1_models_1_1_mapper_factory.html", "class_my_shoe_shop_1_1_web_1_1_models_1_1_mapper_factory" ],
          [ "Shoe", "class_my_shoe_shop_1_1_web_1_1_models_1_1_shoe.html", "class_my_shoe_shop_1_1_web_1_1_models_1_1_shoe" ],
          [ "ShoeListViewModel", "class_my_shoe_shop_1_1_web_1_1_models_1_1_shoe_list_view_model.html", "class_my_shoe_shop_1_1_web_1_1_models_1_1_shoe_list_view_model" ]
        ] ],
        [ "Program", "class_my_shoe_shop_1_1_web_1_1_program.html", "class_my_shoe_shop_1_1_web_1_1_program" ],
        [ "Startup", "class_my_shoe_shop_1_1_web_1_1_startup.html", "class_my_shoe_shop_1_1_web_1_1_startup" ]
      ] ]
    ] ],
    [ "MyShoeShopGui", "namespace_my_shoe_shop_gui.html", [
      [ "BL", "namespace_my_shoe_shop_gui_1_1_b_l.html", [
        [ "IEditorService", "interface_my_shoe_shop_gui_1_1_b_l_1_1_i_editor_service.html", "interface_my_shoe_shop_gui_1_1_b_l_1_1_i_editor_service" ],
        [ "IShoeLogic", "interface_my_shoe_shop_gui_1_1_b_l_1_1_i_shoe_logic.html", "interface_my_shoe_shop_gui_1_1_b_l_1_1_i_shoe_logic" ],
        [ "ShoeLogic", "class_my_shoe_shop_gui_1_1_b_l_1_1_shoe_logic.html", "class_my_shoe_shop_gui_1_1_b_l_1_1_shoe_logic" ]
      ] ],
      [ "DATA", "namespace_my_shoe_shop_gui_1_1_d_a_t_a.html", [
        [ "Cipo", "class_my_shoe_shop_gui_1_1_d_a_t_a_1_1_cipo.html", "class_my_shoe_shop_gui_1_1_d_a_t_a_1_1_cipo" ]
      ] ],
      [ "UI", "namespace_my_shoe_shop_gui_1_1_u_i.html", [
        [ "EditorServiceViaWindow", "class_my_shoe_shop_gui_1_1_u_i_1_1_editor_service_via_window.html", "class_my_shoe_shop_gui_1_1_u_i_1_1_editor_service_via_window" ]
      ] ],
      [ "VM", "namespace_my_shoe_shop_gui_1_1_v_m.html", [
        [ "EditorViewModel", "class_my_shoe_shop_gui_1_1_v_m_1_1_editor_view_model.html", "class_my_shoe_shop_gui_1_1_v_m_1_1_editor_view_model" ],
        [ "MainViewModel", "class_my_shoe_shop_gui_1_1_v_m_1_1_main_view_model.html", "class_my_shoe_shop_gui_1_1_v_m_1_1_main_view_model" ]
      ] ],
      [ "App", "class_my_shoe_shop_gui_1_1_app.html", "class_my_shoe_shop_gui_1_1_app" ],
      [ "EditorWindow", "class_my_shoe_shop_gui_1_1_editor_window.html", "class_my_shoe_shop_gui_1_1_editor_window" ],
      [ "MainWindow", "class_my_shoe_shop_gui_1_1_main_window.html", "class_my_shoe_shop_gui_1_1_main_window" ],
      [ "MyIOC", "class_my_shoe_shop_gui_1_1_my_i_o_c.html", "class_my_shoe_shop_gui_1_1_my_i_o_c" ]
    ] ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", "class_xaml_generated_namespace_1_1_generated_internal_type_helper" ]
    ] ]
];