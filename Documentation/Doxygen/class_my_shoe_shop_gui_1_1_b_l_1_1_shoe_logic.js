var class_my_shoe_shop_gui_1_1_b_l_1_1_shoe_logic =
[
    [ "ShoeLogic", "class_my_shoe_shop_gui_1_1_b_l_1_1_shoe_logic.html#a43d71aace4ecfcb2435046a492e1cd1c", null ],
    [ "AddShoe", "class_my_shoe_shop_gui_1_1_b_l_1_1_shoe_logic.html#a1ff59c7f0887d1a8467cf2d29cbd8922", null ],
    [ "DelShoe", "class_my_shoe_shop_gui_1_1_b_l_1_1_shoe_logic.html#a332f0280a09247d0a4ec130471171f62", null ],
    [ "GetAllShoes", "class_my_shoe_shop_gui_1_1_b_l_1_1_shoe_logic.html#ac2579dcd1ddebafec76f127aaae32569", null ],
    [ "ModShoe", "class_my_shoe_shop_gui_1_1_b_l_1_1_shoe_logic.html#a89bc28e37aa04868e13fef2c953c00b7", null ]
];