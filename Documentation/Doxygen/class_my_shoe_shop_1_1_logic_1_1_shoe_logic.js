var class_my_shoe_shop_1_1_logic_1_1_shoe_logic =
[
    [ "ShoeLogic", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#ab9449ea18c589c9728db20eb2627567b", null ],
    [ "ShoeLogic", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#ac7259d8fbac5610306e32efafd664d7a", null ],
    [ "CreateBeszallito", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#a2b69c5d130353b71af7d6d16486ff0f6", null ],
    [ "CreateCipo", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#a47317cbffe625ad2d94d150c495f5b3e", null ],
    [ "CreateVasarlo", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#ac602f8d1cf56e3f60eea0558de841f38", null ],
    [ "DeleteBeszallito", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#aa30619ae34055c51c2d31633d2110650", null ],
    [ "DeleteCipo", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#ad1cd0746feb37aac266eb7638ee89563", null ],
    [ "DeleteVasarlo", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#a63de11884bf6c41d300ab1a8305c6630", null ],
    [ "ReadAllBeszallito", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#aee5b8180a9c49922b99d9e618a60e830", null ],
    [ "ReadAllCipo", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#acb57581a5a4ab740714188aae7ea31af", null ],
    [ "ReadAllVasarlo", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#af301f4614ddf070c5b69d551b0982085", null ],
    [ "ReadBeszallito", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#a45c108f54d99be3f25266c12c1a2e330", null ],
    [ "ReadCipo", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#a2660d4f8157870b707085da274e273ae", null ],
    [ "ReadVasarlo", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#a54ff3c7d9be2b8c5c5dc4f7c6fbd5f63", null ],
    [ "UpdateBeszallitoNepszeruseg", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#a84bbe70b4ff3c07955fe6d7d8d3d3649", null ],
    [ "UpdateCipo", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#aaca8389f0c473c2fdd4a0c63134b083f", null ],
    [ "UpdateVasarloCipo", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#af2953718a9588d6e82f939c45c3ea8f0", null ]
];