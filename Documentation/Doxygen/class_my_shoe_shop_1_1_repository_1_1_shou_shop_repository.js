var class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository =
[
    [ "ShouShopRepository", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#a59a05dd0b36a142dea180ad848df752b", null ],
    [ "Create", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#a52ce959e60e60409cb53199f93d3678e", null ],
    [ "Create", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#aeb5a800181878a4136bcb6c8c51ddc7c", null ],
    [ "Create", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#a27b9c2e6a9bf2565f6b9b41f3991939f", null ],
    [ "DeleteBeszallito", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#a71fa4db6d95dbfb5b0e244b3102d6c9d", null ],
    [ "DeleteCipo", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#a87dac6f365554a18d9236ca11c4d867a", null ],
    [ "DeleteVasarlo", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#abb3d130e08d72008d095e26cd1394c5c", null ],
    [ "ReadAllBeszallito", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#a54a56143c969897f095b9bbfbd0052de", null ],
    [ "ReadAllCipo", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#a0f03abe886a8021946de2b32c4fa1d40", null ],
    [ "ReadAllVasarlo", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#ae16712051b225b8021275c6172f4d803", null ],
    [ "UpdateBeszallitoNepszeruseg", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#a233b2d040c5cac01990bad7d09c88d07", null ],
    [ "UpdateCipo", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#a08ca330b0c6b4246850c01deca7b3ce8", null ],
    [ "UpdateVasarloCipo", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#ab26b3da3b26cf2012876d17670122473", null ]
];