var hierarchy =
[
    [ "Application", null, [
      [ "MyShoeShopGui.App", "class_my_shoe_shop_gui_1_1_app.html", null ]
    ] ],
    [ "System.Windows.Application", null, [
      [ "MyShoeShopGui.App", "class_my_shoe_shop_gui_1_1_app.html", null ],
      [ "MyShoeShopGui.App", "class_my_shoe_shop_gui_1_1_app.html", null ],
      [ "MyShoeShopGui.App", "class_my_shoe_shop_gui_1_1_app.html", null ]
    ] ],
    [ "MyShoeShop.Data.Models.Beszallito", "class_my_shoe_shop_1_1_data_1_1_models_1_1_beszallito.html", null ],
    [ "MyShoeShop.Data.Models.Cipo", "class_my_shoe_shop_1_1_data_1_1_models_1_1_cipo.html", null ],
    [ "Controller", null, [
      [ "MyShoeShop.Web.Controllers.HomeController", "class_my_shoe_shop_1_1_web_1_1_controllers_1_1_home_controller.html", null ],
      [ "MyShoeShop.Web.Controllers.ShoesController", "class_my_shoe_shop_1_1_web_1_1_controllers_1_1_shoes_controller.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "MyShoeShop.Data.Models.ShoeDatabaseContext", "class_my_shoe_shop_1_1_data_1_1_models_1_1_shoe_database_context.html", null ]
    ] ],
    [ "MyShoeShop.Web.Models.ErrorViewModel", "class_my_shoe_shop_1_1_web_1_1_models_1_1_error_view_model.html", null ],
    [ "System.Windows.Markup.IComponentConnector", null, [
      [ "MyShoeShopGui.EditorWindow", "class_my_shoe_shop_gui_1_1_editor_window.html", null ],
      [ "MyShoeShopGui.EditorWindow", "class_my_shoe_shop_gui_1_1_editor_window.html", null ],
      [ "MyShoeShopGui.EditorWindow", "class_my_shoe_shop_gui_1_1_editor_window.html", null ],
      [ "MyShoeShopGui.MainWindow", "class_my_shoe_shop_gui_1_1_main_window.html", null ],
      [ "MyShoeShopGui.MainWindow", "class_my_shoe_shop_gui_1_1_main_window.html", null ],
      [ "MyShoeShopGui.MainWindow", "class_my_shoe_shop_gui_1_1_main_window.html", null ]
    ] ],
    [ "MyShoeShopGui.BL.IEditorService", "interface_my_shoe_shop_gui_1_1_b_l_1_1_i_editor_service.html", [
      [ "MyShoeShopGui.UI.EditorServiceViaWindow", "class_my_shoe_shop_gui_1_1_u_i_1_1_editor_service_via_window.html", null ]
    ] ],
    [ "System.Windows.Markup.InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "MyShoeShop.Repository.IRepository< T >", "interface_my_shoe_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "MyShoeShop.Repository.IRepository< Cipo >", "interface_my_shoe_shop_1_1_repository_1_1_i_repository.html", [
      [ "MyShoeShop.Repository.IShoeShopRepository", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html", [
        [ "MyShoeShop.Repository.ShouShopRepository", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html", null ]
      ] ]
    ] ],
    [ "IServiceLocator", null, [
      [ "MyShoeShopGui.MyIOC", "class_my_shoe_shop_gui_1_1_my_i_o_c.html", null ]
    ] ],
    [ "MyShoeShop.Logic.IShoeLogic", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html", [
      [ "MyShoeShop.Logic.ShoeLogic", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html", null ]
    ] ],
    [ "MyShoeShopGui.BL.IShoeLogic", "interface_my_shoe_shop_gui_1_1_b_l_1_1_i_shoe_logic.html", [
      [ "MyShoeShopGui.BL.ShoeLogic", "class_my_shoe_shop_gui_1_1_b_l_1_1_shoe_logic.html", null ]
    ] ],
    [ "MyShoeShop.Web.Models.MapperFactory", "class_my_shoe_shop_1_1_web_1_1_models_1_1_mapper_factory.html", null ],
    [ "ObservableObject", null, [
      [ "MyShoeShopGui.DATA.Cipo", "class_my_shoe_shop_gui_1_1_d_a_t_a_1_1_cipo.html", null ]
    ] ],
    [ "MyShoeShop.Web.Program", "class_my_shoe_shop_1_1_web_1_1_program.html", null ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage", null, [
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views_Shoes_ShoesDetails", "class_asp_net_core_1_1_views___shoes___shoes_details.html", null ],
      [ "AspNetCore.Views_Shoes_ShoesDetails", "class_asp_net_core_1_1_views___shoes___shoes_details.html", null ],
      [ "AspNetCore.Views_Shoes_ShoesEdit", "class_asp_net_core_1_1_views___shoes___shoes_edit.html", null ],
      [ "AspNetCore.Views_Shoes_ShoesEdit", "class_asp_net_core_1_1_views___shoes___shoes_edit.html", null ],
      [ "AspNetCore.Views_Shoes_ShoesIndex", "class_asp_net_core_1_1_views___shoes___shoes_index.html", null ],
      [ "AspNetCore.Views_Shoes_ShoesIndex", "class_asp_net_core_1_1_views___shoes___shoes_index.html", null ],
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ]
    ] ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage< IEnumerable< MyShoeShop.Web.Models.Shoe >>", null, [
      [ "AspNetCore.Views_Shoes_ShoesList", "class_asp_net_core_1_1_views___shoes___shoes_list.html", null ],
      [ "AspNetCore.Views_Shoes_ShoesList", "class_asp_net_core_1_1_views___shoes___shoes_list.html", null ]
    ] ],
    [ "MyShoeShop.Web.Models.Shoe", "class_my_shoe_shop_1_1_web_1_1_models_1_1_shoe.html", null ],
    [ "MyShoeShop.Web.Models.ShoeListViewModel", "class_my_shoe_shop_1_1_web_1_1_models_1_1_shoe_list_view_model.html", null ],
    [ "SimpleIoc", null, [
      [ "MyShoeShopGui.MyIOC", "class_my_shoe_shop_gui_1_1_my_i_o_c.html", null ]
    ] ],
    [ "MyShoeShop.Web.Startup", "class_my_shoe_shop_1_1_web_1_1_startup.html", null ],
    [ "MyShoeShop.Data.Models.Vasarlo", "class_my_shoe_shop_1_1_data_1_1_models_1_1_vasarlo.html", null ],
    [ "ViewModelBase", null, [
      [ "MyShoeShopGui.VM.EditorViewModel", "class_my_shoe_shop_gui_1_1_v_m_1_1_editor_view_model.html", null ],
      [ "MyShoeShopGui.VM.MainViewModel", "class_my_shoe_shop_gui_1_1_v_m_1_1_main_view_model.html", null ]
    ] ],
    [ "System.Windows.Window", null, [
      [ "MyShoeShopGui.EditorWindow", "class_my_shoe_shop_gui_1_1_editor_window.html", null ],
      [ "MyShoeShopGui.EditorWindow", "class_my_shoe_shop_gui_1_1_editor_window.html", null ],
      [ "MyShoeShopGui.EditorWindow", "class_my_shoe_shop_gui_1_1_editor_window.html", null ],
      [ "MyShoeShopGui.MainWindow", "class_my_shoe_shop_gui_1_1_main_window.html", null ],
      [ "MyShoeShopGui.MainWindow", "class_my_shoe_shop_gui_1_1_main_window.html", null ],
      [ "MyShoeShopGui.MainWindow", "class_my_shoe_shop_gui_1_1_main_window.html", null ],
      [ "MyShoeShopGui.MainWindow", "class_my_shoe_shop_gui_1_1_main_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "MyShoeShopGui.EditorWindow", "class_my_shoe_shop_gui_1_1_editor_window.html", null ]
    ] ]
];